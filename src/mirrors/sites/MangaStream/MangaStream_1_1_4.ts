import { BaseMirror } from "../abstract/BaseMirror"
import { MirrorImplementation, MirrorObject } from "../../../types/common"
import { MirrorHelper } from "../../MirrorHelper"
import ManhwaxIcon from "../../icons/manhwax-optimized.png"
import NightScansIcon from "../../icons/night-scans-optimized.png"
import AnigliScansIcon from "../../icons/anigli-scans-optimized.png"

const defaultOptions = {
    base_url: "",
    series_list_selector: '.listupd a[href*="/manga/"]',
    chapter_list_selector: ".eph-num a",
    manga_url_selector: ".allc a",
    img_sel: "#readerarea img",
    urlProcessorSeries: url => url,
    urlProcessorChapter: url => url
}

export class MangaStream_1_1_4 extends BaseMirror implements MirrorImplementation {
    canListFullMangas = false

    chapter_url: RegExp
    domains: string[]
    home: string
    languages: string
    mirrorIcon: string
    mirrorName: string
    disabled: boolean | undefined

    private readonly options: typeof defaultOptions

    constructor(mirrorHelper: MirrorHelper, mirror: MirrorObject, options: Partial<typeof defaultOptions> = {}) {
        super(mirrorHelper)

        this.mirrorName = mirror.mirrorName
        this.mirrorIcon = mirror.mirrorIcon
        this.domains = mirror.domains
        this.home = mirror.home
        this.chapter_url = mirror.chapter_url
        this.languages = mirror.languages
        this.disabled = mirror.disabled

        this.options = {
            ...defaultOptions,
            ...options
        }
    }

    public async getMangaList(search: string) {
        const res = []
        const urlManga = this.options.base_url + "?s=" + search
        const doc = await this.mirrorHelper.loadPage(urlManga, { nocache: true, preventimages: true })
        const self = this
        const $ = this.parseHtml(doc)
        $(this.options.series_list_selector, doc).each(function (index) {
            res.push([$(this).find(".tt").text().trim(), self.options.urlProcessorSeries($(this).attr("href"))])
        })
        return res
    }

    public async getListChaps(urlManga: string) {
        const doc = await this.mirrorHelper.loadPage(urlManga, { nocache: true, preventimages: true })
        const res = []
        const self = this
        const $ = this.parseHtml(doc)
        $(this.options.chapter_list_selector, doc).each(function (index) {
            res.push([$(".chapternum", this).text().trim(), self.options.urlProcessorChapter($(this).attr("href"))])
        })
        return res
    }

    public async getCurrentPageInfo(doc, curUrl) {
        const $ = this.parseHtml(doc)
        const title = $(this.options.manga_url_selector, doc)
        return {
            name: title.text(),
            currentMangaURL: this.options.urlProcessorSeries(title.attr("href")),
            currentChapterURL: this.options.urlProcessorChapter(curUrl)
        }
    }

    public async getListImages(doc) {
        const res = []
        const regex = /ts_reader\.run\((.*?)\);/g
        const parts = doc.match(regex)

        if (!Array.isArray(parts) || parts.length === 0) {
            const $ = this.parseHtml(doc)
            const res: string[] = []
            $(this.options.img_sel, doc).each(function (index) {
                res[res.length] = $(this).attr("src")
            })
            return res
        }

        const json = JSON.parse(parts[0].replace("ts_reader.run(", "").replace(");", ""))

        for (const source of json.sources) {
            // Only need one source, as others are duplicate
            if (source.images) {
                return source.images
            }
        }
        return res
    }

    public async getImageUrlFromPage(urlImg) {
        return urlImg
    }

    public isCurrentPageAChapterPage(doc: string) {
        return this.queryHtml(doc, "div#readerarea").length > 0
    }
}

export const getMangaStream114Implementations = (mirrorHelper: MirrorHelper): MirrorImplementation[] => {
    return [
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Manhwax",
                mirrorIcon: ManhwaxIcon,
                languages: "en",
                domains: ["manhwax.com", "manhwax.org"],
                home: "https://manhwax.org/",
                chapter_url: /\/.*?chapter-[0-9]+.*\//g
            },
            {
                base_url: "https://manhwax.org/"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Night Scans",
                canListFullMangas: false,
                mirrorIcon: NightScansIcon,
                domains: ["nightscans.org", "nightscans.net", "night-scans.com", "nightsup.net"],
                home: "https://nightsup.net",
                chapter_url: /\/.*?chapter-[0-9]+.*\//g,
                languages: "en"
            },
            {
                // search_url: "https://nightscans.org",
                chapter_list_selector: "#chapterlist .eph-num a"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Animated Glitched Scans",
                canListFullMangas: false,
                mirrorIcon: AnigliScansIcon,
                domains: ["anigliscans.com", "anigliscans.xyz"],
                home: "https://anigliscans.xyz",
                chapter_url: /.*/g,
                languages: "en"
            },
            {
                // search_url: "https://anigliscans.xyz"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Lynx Scans",
                mirrorIcon: require("../../icons/lynxscans-optimized.png"),
                languages: "en",
                domains: ["lynxscans.com"],
                home: "https://lynxscans.com/home",
                canListFullMangas: true,
                chapter_url: /^\/*\/.+$/g
            },
            {}
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Rizz Comic",
                canListFullMangas: false,
                mirrorIcon: require("../../icons/rizz-comic-optimized.png"),
                domains: ["rizzcomic.com", "rizzfables.com"],
                home: "https://rizzfables.com",
                chapter_url: /chapter-[0-9]/g,
                languages: "en"
            },
            {
                // search_url: "https://realmscans.xyz/",
                // series_list_selector: '.listupd a[href*="/series/"]',
                chapter_list_selector: `.eph-num a[href*="rizzfables.com"]`
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Surya Toon",
                mirrorIcon: require("../../icons/surya-toon-optimized.png"),
                languages: "en",
                domains: ["suryatoon.com"],
                home: "https://suryatoon.com",
                // canListFullMangas: true,
                chapter_url: /^\/*\/.+$/g
            },
            {
                base_url: "https://suryatoon.com/",
                chapter_list_selector: "#chapterlist .eph-num a"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Manga Galaxy",
                mirrorIcon: require("../../icons/manga-galaxy-optimized.png"),
                languages: "en",
                domains: ["mangagalaxy.me", "mangagalaxy.org"],
                home: "https://mangagalaxy.org",
                // canListFullMangas: true,
                chapter_url: /chapter\-\d+/g
            },
            {
                base_url: "https://mangagalaxy.org/",
                chapter_list_selector: "#chapterlist .eph-num a"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Drake Comic",
                mirrorIcon: require("../../icons/drake-comic-optimized.png"),
                languages: "en",
                domains: ["drakecomic.com", "drakecomic.org"],
                home: "https://drakecomic.org",
                // canListFullMangas: true,
                chapter_url: /.*\-\d+/g
            },
            {
                base_url: "https://drakecomic.org/"
                // chapter_list_selector: "#chapterlist .eph-num a"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Cypher Scans",
                mirrorIcon: require("../../icons/cypher-scans-optimized.png"),
                languages: "en",
                domains: ["cypherscans.xyz", "cypho-scans.xyz"],
                home: "https://cypho-scans.xyz",
                // canListFullMangas: true,
                chapter_url: /chapter\-\d+/g
            },
            {
                base_url: "https://cypho-scans.xyz",
                chapter_list_selector: "#chapterlist .eph-num a"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Raw Kuma",
                canListFullMangas: false,
                mirrorIcon: require("../../icons/raw-kuma-optimized.png"),
                domains: ["rawkuma.com"],
                home: "https://rawkuma.com/",
                chapter_url: /chapter-[0-9]+.*\/$/g,
                languages: ["jp", "cn", "hk", "kr"].join(",")
            },
            {}
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Thunder Scans",
                canListFullMangas: false,
                mirrorIcon: require("../../icons/thunder-scans-optimized.png"),
                domains: ["en-thunderscans.com"],
                home: "https://en-thunderscans.com/",
                chapter_url: /chapter-[0-9]+.*\/$/g,
                languages: "en"
            },
            {
                chapter_list_selector: "#chapterlist li a:not([data-coin])"
            }
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Kappa Beast",
                canListFullMangas: false,
                mirrorIcon: require("../../icons/kappa-beast-optimized.png"),
                domains: ["kappabeast.com"],
                home: "https://kappabeast.com/",
                chapter_url: /.*\-\d+/g,
                languages: "en"
            },
            {}
        ),
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Surya Scans",
                canListFullMangas: false,
                mirrorIcon: require("../../icons/surya-scans-optimized.png"),
                domains: ["suryascans.org"],
                home: "https://suryascans.org/",
                chapter_url: /chapter-[0-9]+.*\/$/g,
                languages: "en"
            },
            {}
        )
        /*,
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "Realm Oasis",
                canListFullMangas: false,
                mirrorIcon: require("../../icons/realm-oasis-optimized.png"),
                domains: ["realmoasis.com"],
                home: "https://realmoasis.com",
                chapter_url: /.*\/.* /g,
                languages: "en"
            },
            {
                base_url: "https://realmoasis.com/",
                // series_list_selector: '.listupd a[href*="/series/"]',
                chapter_list_selector: "#chapterlist .eph-num a"
            }
        ) */
        /*,
        new MangaStream_1_1_4(
            mirrorHelper,
            {
                mirrorName: "GenZ Toons",
                mirrorIcon: require("../../icons/gen-z-toons-optimized.png"),
                languages: "en",
                domains: ["genztoons.com"],
                home: "https://genztoons.com",
                // canListFullMangas: true,
                chapter_url: /^\/*\/.+$/g
            },
            {
                base_url: "https://genztoons.com/",
                chapter_list_selector: "#chapterlist .eph-num a"
            }
        )*/
    ]
}
